//
//  CustomTableViewCell.swift
//  UITableViewController
//
//  Created by Родион Шашенко on 01.04.2023.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLable: UILabel!
    
    @IBOutlet weak var ageLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func setCell(with frend: Model){
        nameLable.text = frend.firstName
        ageLable.text = frend.age
        ageLable.text = ageLable.text! + " age"
    }

}
